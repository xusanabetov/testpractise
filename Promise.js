console.log('Request data...');

// setTimeout(()=>{
//  console.log('Preparing data...')

//  const backendData={
//      server:'aws',
//      port:2000,
//      status:'working'
//     }

//     setTimeout(()=>{
//     backendData.modified=true
//     console.log('Data received',backendData)
//     },2000)
// },2000)

const p = new Promise(function(resolve,reject){
   setTimeout(()=>{
       console.log('Preparing data...')
       const backendData={
           server:'beeline',
           port:4000,
           status:'working'
       }
       resolve(backendData)
    },2000) 
}) 

p.then((data)=>{
  return new Promise((resolve,reject)=>{
      setTimeout(()=>{
        data.modified=true
        resolve(data)
        //console.log('Data received',backendData);
      },2000)
  })
})
  .then(clientData=>{
      clientData.fromPromise=true
      return clientData
  }).then(data=>{
      console.log('Modified',data);
  })
  .catch(err=>console.error('Error: ',err))
  .finally(()=>console.log('Finally'))


const sleep=ms=>{
    return new Promise(resolve=>{
        setTimeout(()=>resolve("Promise№1"),ms)
    })
}

const sleep1=ms=>{
    return new Promise(resolve=>{
        setTimeout(()=>resolve("Promise№2"),ms)
    })
}

sleep(3000).then((data)=>console.log(data));
sleep1(1000).then((data)=>console.log(data));


Promise.all([sleep(2000),sleep(5000)]).then(()=>{
    console.log('All promises');
})

Promise.race([sleep(2000),sleep(5000)]).then(()=>{
    console.log('Race promises');
})