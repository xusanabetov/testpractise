const football_team=[
    {name:"Real Madrid", created:1980,budget:2400000},
    {name:"Barcelona", created:1870,budget:1340000},
    {name:"Chelsea", created:1970,budget:45698990},
    {name:"Arsenal", created:2001,budget:45223212},
    {name:"Tottenhem", created:1945,budget:302322213},
    {name:"Ufa", created:2010,budget:9583223}
]

// for(let i=0;i<football_team.length;i++){
//     console.log(football_team[i])
// }

// for (let team of football_team){
//     console.log(team);
// }

// football_team.forEach(function(team,index,tArr){
//     console.log(team);
//     console.log(index);
//     console.log(tArr);
// })


// football_team.forEach(person=>console.log(person));


// const newPeople=football_team.map(team=>`${team.name} (${team.budget+10})`)
// console.log(newPeople);

// const adults=[];
// for(let i=0;i<football_team.length;i++){
//     if(football_team[i].budget>=19000000){
//         adults.push(football_team[i])
//     }
//     // console.log(football_team[i])
// }
// console.log(adults);


// const adults=football_team.filter(team=>team.budget>=19000000)
// console.log(adults);


// const amount=football_team.reduce((total,team)=>total+team.budget,0)
// console.log(amount);


// const teams=football_team.find(team=>team.name=="Chelsea")
// console.log(teams);

const amount=football_team
.filter(team=>team.budget>3000)
.map(team=>{
    return {
       info:`${team.name} (${team.created})`,
       budget: Math.sqrt(team.budget)
    }
})
// .reduce((total,team)=>total+team.budget,0)

console.log(amount);