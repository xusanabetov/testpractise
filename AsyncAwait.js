function resolveAfter2Seconds(x){
    return new Promise(resolve=>{
        setTimeout(() => {
            resolve(x)
        }, 2000);
    })
}

async function add1(x){
    const a=await resolveAfter2Seconds(20);
    const b=await resolveAfter2Seconds(30);
    return x+a+b;
}

add1(10).then(v=>{
    console.log(v);
})


async function add2(x){
    const a=resolveAfter2Seconds(20);
    const b=resolveAfter2Seconds(30);

    return x+await a+await b;
}

add2(10).then(v=>{
    console.log(v);
})


async function throwsValue(){
    throw new Error('oops');
}


throwsValue()
    .then((resolve) => {
        console.log("resolve:" + resolve);
    })
    .catch((reject) => {
        console.log("reject:" + reject);
    });


function getUser(id){
    return fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
    .then(response=>response.json());
    //return Promise.resolve({ id: 1 });
    //throw new Error('Ooops');
}

class DataService{
    constructor(url){
        this.url=url;
    }

    async getUser(id){
        try{
            let response=await fetch(`${this.url}/users/${id}`);
            let data=await response.json();
            return data;
            } catch(error){
                throw new Error('Не удалось получить данные пользователя');
            }
    }

    async getPosts(userId){
    try{
    let response=await fetch(`${this.url}/posts?userId=/${userId}`);
    let data=await response.json();
    return data;
    } catch(error){
        throw new Error('Не удалось получить посты');
    }
    }

    
    async getComments(postId){
        try{
        let response=await fetch(`${this.url}/comments?postId=/${postId}`);
        let data=await response.json();
        return data;
        } catch(error){
            throw new Error('Не удалось получить комментарии');
        }
        }
};

// const getUser=async (id)=>{
//     try{
//     let response=await fetch(`https://jsonplaceholder.typicode.com/users/${id}`);
//     let data=await response.json();
  
//     return data;
//     } catch(error){
//         throw new Error('Не удалось получить данные от сервера');
//     }
// }

// getUser(1)
// .then(user=>console.log(user))
// .catch(error=>console.log(error))


(async ()=>{
    let dataService=new DataService('https://jsonplaceholder.typicode.com');
    try{
      let user=await dataService.getUser(1);
      let posts=await dataService.getPosts(user.id);
      let comments=await dataService.getComments(posts[0].id);
      console.log(user);
    }
    catch(error){
      console.error(error);
    }
})();

// main();
